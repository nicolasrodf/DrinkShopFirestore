package com.nicolasrf.drinkshopfirestore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nex3z.notificationbadge.NotificationBadge;
import com.nicolasrf.drinkshopfirestore.Adapter.CategoryAdapter;
import com.nicolasrf.drinkshopfirestore.Adapter.MainSliderAdapter;
import com.nicolasrf.drinkshopfirestore.Database.DataSource.CartRepository;
import com.nicolasrf.drinkshopfirestore.Database.DataSource.FavoriteRepository;
import com.nicolasrf.drinkshopfirestore.Database.Local.CartDataSource;
import com.nicolasrf.drinkshopfirestore.Database.Local.FavoriteDataSource;
import com.nicolasrf.drinkshopfirestore.Database.Local.NICORoomDatabase;
import com.nicolasrf.drinkshopfirestore.Model.Banner;
import com.nicolasrf.drinkshopfirestore.Model.Category;
import com.nicolasrf.drinkshopfirestore.Model.CheckUserResponse;
import com.nicolasrf.drinkshopfirestore.Model.Drink;
import com.nicolasrf.drinkshopfirestore.Model.User;
import com.nicolasrf.drinkshopfirestore.Retrofit.IDrinkShopAPI;
import com.nicolasrf.drinkshopfirestore.Services.PicassoImageLoadingService;
import com.nicolasrf.drinkshopfirestore.Utils.Common;
import com.nicolasrf.drinkshopfirestore.Utils.UploadCallBack;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UploadCallBack {
    private static final String TAG = "HomeActivity";

    @Override
    public void onProgressUpdate(int percentage) {

    }

    private static final int PICK_FILE_REQUEST = 1222;
    private final int REQUEST_LOGIN = 2000;

    TextView nameTextView, phoneTextView;
    SliderLayout sliderLayout;

    List<Banner> banners = new ArrayList<>();

    FirebaseAuth auth;

    Menu nav_Menu;

    IDrinkShopAPI mService;

    RecyclerView menuListRecycler;

    NotificationBadge badge;
    ImageView cartIconImageView;
    CircleImageView avatarImageView;

    Uri selectedFileUri = null;
    String uploadedImgPath="";
    private File compressedImageFile;

    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    SwipeRefreshLayout swipeRefreshLayout;

    //RxJava
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    Uri downloadUri;

    Slider slider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d(TAG, "onCreate: STARTED");

        setTitle("READY BEBIDAS");

        mService = Common.getAPI();

        auth = FirebaseAuth.getInstance();

        Slider.init(new PicassoImageLoadingService(this));

        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        menuListRecycler = findViewById(R.id.recycler_drinks);
        menuListRecycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        menuListRecycler.setHasFixedSize(true);

        //sliderLayout = findViewById(R.id.slider);

        swipeRefreshLayout = findViewById(R.id.swipe_layout);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        nav_Menu = navigationView.getMenu();

        nameTextView = headerView.findViewById(R.id.name_text_view);
        phoneTextView = headerView.findViewById(R.id.phone_text_view);
        avatarImageView =headerView.findViewById(R.id.avatar_image_view);

        //Event
        avatarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.currentUser != null)
                    checkPermissions();
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getBannerImage();

                getMenu();

                getToppingList();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getBannerImage();

                getMenu();

                getToppingList();
            }
        });

        //Getr banner
        getBannerImage();

        //Get menu
        getMenu();

        //Save newest Topping list
        getToppingList();

        //Init Database
        initDB();

        //If user already loged, just login again (Session still live)
        checkSessionLogin();

        slider = findViewById(R.id.banner_slider1);
        slider.setAdapter(new MainSliderAdapter());
    }


    private void checkSessionLogin() {

        if(auth.getCurrentUser()!= null) {

            swipeRefreshLayout.setRefreshing(true);

            //if already login
            if (!FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().isEmpty()) {

                Toast.makeText(HomeActivity.this, "USER PHONE " + FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber(), Toast.LENGTH_SHORT).show();


                DocumentReference docRef = firebaseFirestore.collection("Users").document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());

                                Common.currentUser = document.toObject(User.class);

                                if (Common.currentUser != null) {

                                    swipeRefreshLayout.setRefreshing(false);

                                    if (Common.currentUser != null) {

                                        nameTextView.setText(Common.currentUser.getName());
                                        phoneTextView.setText(Common.currentUser.getPhone());

                                        nav_Menu.findItem(R.id.nav_sign_out).setVisible(true); //activar vista de sign out
                                        nav_Menu.findItem(R.id.nav_show_orders).setVisible(true); //activar vista de orders
                                        nav_Menu.findItem(R.id.nav_settings).setVisible(true); //activar vista de settings

                                        if (!TextUtils.isEmpty(Common.currentUser.getAvatarUrl())) {
                                            Picasso.with(getBaseContext())
                                                    .load(Common.currentUser.getAvatarUrl())
                                                    .into(avatarImageView);
                                        }
                                    }
                                }

                            } else {
                                Log.d(TAG, "No such document");
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });


            }
        }
    }


    //Crtl+O
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                avatarImageView.setImageURI(selectedFileUri);
                uploadFileToServer();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(HomeActivity.this, "Error en recortar la imagen.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(HomeActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }


    private void uploadFileToServer() {

        if(selectedFileUri != null){

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(HomeActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/profile/"+Common.currentUser.getPhone()+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Toast.makeText(HomeActivity.this, "Upload failed."+ exception.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(HomeActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(HomeActivity.this, "Upload: " + taskSnapshot.getBytesTransferred()*0.01, Toast.LENGTH_SHORT).show();
                }
            });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);
                        downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Common.currentUser.setAvatarUrl(downloadUri.toString());

                        //Upload to firestore
                        firebaseFirestore.collection("Users").document(Common.currentUser.getPhone())
                                .update("avatarUrl", downloadUri.toString())
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                                        Toast.makeText(HomeActivity.this, "Foto actualizada", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(HomeActivity.this, "Error updating image", Toast.LENGTH_SHORT).show();
                                        Log.w(TAG, "Error updating document", e);
                                    }
                                });



                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });

        }
    }

    private void initDB() {
        Common.nicoRoomDatabase = NICORoomDatabase.getInstance(this);
        Common.cartRepository = CartRepository.getInstance(CartDataSource.getInstance(Common.nicoRoomDatabase.cartDAO()));
        Common.favoriteRepository = FavoriteRepository.getInstance(FavoriteDataSource.getInstance(Common.nicoRoomDatabase.favoriteDAO()));
    }

    private void getToppingList() {

        final List<Drink> drinks = new ArrayList<>();

        firebaseFirestore.collection("Menu/" + Common.TOPPING_MENU_ID + "/Drinks")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                Drink drink = document.toObject(Drink.class).withId(document.getId());
                                drinks.add(drink);

                            }

                            Common.toppingList = drinks;

                        }

                    }

                });
    }

    private void getMenu() {

        final List<Category> categories = new ArrayList<>();

        firebaseFirestore.collection("Menu")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                //Toast.makeText(HomeActivity.this, "name: " + document.get("name"), Toast.LENGTH_SHORT).show();
                                Category category = document.toObject(Category.class).withId(document.getId());
                                categories.add(category);

                            }

                            displayMenu(categories);

                        }

                    }

                });

    }

    private void displayMenu(List<Category> categories) {
        CategoryAdapter adapter = new CategoryAdapter(this,categories);
        menuListRecycler.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void getBannerImage() {

        firebaseFirestore.collection("Banner")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                //Toast.makeText(HomeActivity.this, "name: " + document.get("name"), Toast.LENGTH_SHORT).show();
                                Banner banner = document.toObject(Banner.class);
                                banners.add(banner);

                            }

                            //displayImage(banners);
                            //displayImage();
                            //Toast.makeText(HomeActivity.this, "banner size " + banners.size(), Toast.LENGTH_SHORT).show();


                        }

                    }

                });

    }

    //Crtl+O
    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

//    private void displayImage(List<Banner> banners) {
//        //Toast.makeText(this, "banner size " + banners.size(), Toast.LENGTH_SHORT).show(); //dice Banner Size 6 !!!
//        HashMap<String,String> bannerMap = new HashMap<>();
//        for(Banner item:banners){
//            bannerMap.put(item.getName(),item.getLink());
//        }
//        for(String name:bannerMap.keySet()) {
//            TextSliderView textSliderView = new TextSliderView(this);
//            textSliderView.description(name)
//                    .image(bannerMap.get(name))
//                    .setScaleType(BaseSliderView.ScaleType.Fit);
//
//            sliderLayout.addSlider(textSliderView);
//        }
//
//    }

    //Exit App when click Back button
    boolean isBackButtonClicked = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(isBackButtonClicked){
                super.onBackPressed();
                return;
            }
            this.isBackButtonClicked = true;
            Toast.makeText(this, "Por favor presionar ATRAS para salir de la aplicacion", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        View view = menu.findItem(R.id.cart_menu).getActionView();
        badge = view.findViewById(R.id.badge);
        cartIconImageView = view.findViewById(R.id.cart_icon_image_view);
        cartIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, CartActivity.class));
            }
        });
        updateCartCount();
        return true;
    }

    private void updateCartCount() {
        if(badge == null){
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(Common.cartRepository.countCartItems() == 0){
                    badge.setVisibility(View.INVISIBLE);
                } else {
                    badge.setVisibility(View.VISIBLE);
                    badge.setText(String.valueOf(Common.cartRepository.countCartItems()));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cart_menu) {
            return true;
        }

        else if (id == R.id.search_menu) {
            startActivity(new Intent(HomeActivity.this, SearchActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /* Hide or show nav items is into checkSessionLogin method */

        if (id == R.id.nav_sign_out) {
            // Create confirm Dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setTitle("Salir de mi Cuenta");
            builder.setMessage("Quires salir de tu cuenta?");
            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   // AccountKit.logOut();
                    //Clear all activity
                    AuthUI.getInstance().signOut(HomeActivity.this)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    startActivity(new Intent(HomeActivity.this,MainActivity.class));
                                    finish();
                                }
                            });
                }
            });
            builder.setPositiveButton("CANCELAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            //Dont forget to show dialog
            builder.show();

        } else if (id == R.id.nav_show_orders){
            if(Common.currentUser != null)
                startActivity(new Intent(HomeActivity.this, ShowOrderActivity.class));
            else
                Toast.makeText(this, "Una vez realices la compra podrás ver tus ordenes :)", Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.nav_favorite){
//            if(Common.currentUser != null)
                startActivity(new Intent(HomeActivity.this, FavoriteListActivity.class));
//            else
//                Toast.makeText(this, "Por favor iniciar sesion", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_nearby_store){

            startActivity(new Intent(HomeActivity.this, NearbyStoreActivity.class));

        } else if (id == R.id.nav_settings) {
            showSettingsDialog();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showSettingsDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("CONFIGURACION");

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout_settings = inflater.inflate(R.layout.setting_layout,null);

        final CheckBox subscribeToNewsCheckBox = layout_settings.findViewById(R.id.subscribe_to_news_checkbox);
        //Button logoutButton = layout_settings.findViewById(R.menuId.logout_button);
        final EditText nameEditText = layout_settings.findViewById(R.id.account_name_edit_text);
        final EditText addressEditText = layout_settings.findViewById(R.id.address_edit_text);

        //Add code remember state of Checkbox
        Paper.init(this);
        String isSubscribe = Paper.book().read("sub_new");
        if(isSubscribe == null || TextUtils.isEmpty(isSubscribe) || isSubscribe.equals("false")){
            subscribeToNewsCheckBox.setChecked(false);
        } else {
            subscribeToNewsCheckBox.setChecked(true);
        }

        nameEditText.setText(Common.currentUser.getName());
        addressEditText.setText(Common.currentUser.getAddress());

        alertDialog.setView(layout_settings);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                //Update name and address
                if(!TextUtils.isEmpty(nameEditText.getText()) && !TextUtils.isEmpty(addressEditText.getText())) {
                    updateNameAndAddress(nameEditText.getText().toString(), addressEditText.getText().toString());
                    dialog.dismiss();
                }

                if(subscribeToNewsCheckBox.isChecked()){
                    FirebaseMessaging.getInstance().subscribeToTopic(Common.topicName);
                    //Write value
                    Paper.book().write("sub_new", "true");
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Common.topicName);
                    //Write value
                    Paper.book().write("sub_new", "false");
                }
            }
        });

        alertDialog.show();
    }

    private void updateNameAndAddress(final String name, final String address) {

        //Update only some document fields
        firebaseFirestore.collection("Users").document(Common.currentUser.getPhone())
                .update("name", name,"address",address)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                        Common.currentUser.setName(name);
                        Common.currentUser.setAddress(address);
                        Toast.makeText(HomeActivity.this, "Info successfully updated!", Toast.LENGTH_SHORT).show();
                        getMenu();

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating document", e);
                        Toast.makeText(HomeActivity.this, "Error de conexion", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    protected void onResume(){
        super.onResume();
        updateCartCount();
        isBackButtonClicked = false;
    }

}
