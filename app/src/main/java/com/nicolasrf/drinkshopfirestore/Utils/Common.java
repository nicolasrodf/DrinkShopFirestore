package com.nicolasrf.drinkshopfirestore.Utils;

import com.nicolasrf.drinkshopfirestore.Database.DataSource.CartRepository;
import com.nicolasrf.drinkshopfirestore.Database.DataSource.FavoriteRepository;
import com.nicolasrf.drinkshopfirestore.Database.Local.NICORoomDatabase;
import com.nicolasrf.drinkshopfirestore.Model.Category;
import com.nicolasrf.drinkshopfirestore.Model.Drink;
import com.nicolasrf.drinkshopfirestore.Model.Order;
import com.nicolasrf.drinkshopfirestore.Model.User;
import com.nicolasrf.drinkshopfirestore.Retrofit.FCMClient;
import com.nicolasrf.drinkshopfirestore.Retrofit.IDrinkShopAPI;
import com.nicolasrf.drinkshopfirestore.Retrofit.IFCMService;
import com.nicolasrf.drinkshopfirestore.Retrofit.RetrofitClient;
import com.nicolasrf.drinkshopfirestore.Retrofit.RetrofitScalarsClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nicolas on 16/05/2018.
 */

public class Common {

    public static final String TOPPING_MENU_ID = "3"; //From menu table database
    private static final String FCM_API = "https://fcm.googleapis.com";
    public static User currentUser = null;
    public static Category currentCategory=null;

    public static List<Drink> toppingList = new ArrayList<>();

    public static double toppingPrice = 0.0;
    public static List<String> toppingAdded = new ArrayList<>();

    public static Order currentOrder = null;

    public static String topicName ="News";

    //Hold field
    public static  int sizeOfCup = -1; //-1 : no choose (error) ¿ , 0 : M , 1 : L
    public static int sugar = -1; //-1: no choose (error)
    public static int ice = -1;

    //Database
    public static NICORoomDatabase nicoRoomDatabase;
    public static CartRepository cartRepository;
    public static FavoriteRepository favoriteRepository;

    //FCM
    public static IFCMService getFCMService(){
        return FCMClient.getClient(FCM_API).create(IFCMService.class);
    }

    //For example in emulator "localhost" = 10.0.2.2
    //public static final String BASE_URL = "http://10.0.2.2/drinkshop/";
    public static final String BASE_URL = "https://nicolasrf.000webhostapp.com/drinkshop/";

    public static final String API_TOKEN_URL = "https://nicolasrf.000webhostapp.com/drinkshop/braintree/main.php";

    public static IDrinkShopAPI getAPI(){
        return RetrofitClient.getClient(BASE_URL).create(IDrinkShopAPI.class);
    }

    //TODO. BRAINTREE FUNCIONALITY DOESNT WORKS
    public static IDrinkShopAPI getScalarsAPI(){
        return RetrofitScalarsClient.getScalarsClient(BASE_URL).create(IDrinkShopAPI.class);
    }

    public static String convertCodeToStatus(int orderStatus) {
        switch (orderStatus)    {
            case 0:
                return "Solicitado";
            case 1:
                return "En Proceso";
            case 2:
                return "En Camino";
            case 3:
                return "Finalizado";
            case -1:
                return "Cancelado";

            default:
                return "Error en la Orden";
        }
    }

    public static int convertStatusToCode(String statusString) {
        switch (statusString)    {
            case "Solicitado":
                return 0;
            case "En Proceso":
                return 1;
            case "En Camino":
                return 2;
            case "Finalizado":
                return 3;
            case "Cancelado":
                return -1;

            default:
                return -2;
        }
    }
}
