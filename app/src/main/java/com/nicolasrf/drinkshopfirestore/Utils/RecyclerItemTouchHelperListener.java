package com.nicolasrf.drinkshopfirestore.Utils;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Nicolas on 2/06/2018.
 */

public interface RecyclerItemTouchHelperListener {
    void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
}
