package com.nicolasrf.drinkshopfirestore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.nicolasrf.drinkshopfirestore.Adapter.CartAdapter;
import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Cart;
import com.nicolasrf.drinkshopfirestore.Model.DataMessage;
import com.nicolasrf.drinkshopfirestore.Model.MyResponse;
import com.nicolasrf.drinkshopfirestore.Model.Order;
import com.nicolasrf.drinkshopfirestore.Model.Token;
import com.nicolasrf.drinkshopfirestore.Retrofit.IDrinkShopAPI;
import com.nicolasrf.drinkshopfirestore.Retrofit.IFCMService;
import com.nicolasrf.drinkshopfirestore.Utils.Common;
import com.nicolasrf.drinkshopfirestore.Utils.RecyclerItemTouchHelper;
import com.nicolasrf.drinkshopfirestore.Utils.RecyclerItemTouchHelperListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import dmax.dialog.SpotsDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity implements RecyclerItemTouchHelperListener {
    private static final String TAG = "CartActivity";

    private static final int PAYMENT_REQUEST_CODE = 7777;
    RecyclerView cartRecycler;
    Button placeOrderButton;
    CartAdapter cartAdapter;
    RelativeLayout rootLayout;

    IDrinkShopAPI mService;
    IDrinkShopAPI mServiceScalars;

    private FirebaseFirestore firebaseFirestore;

    //Global String
    String token,amount,orderAddress,orderComment;
    HashMap<String,String> params;

    List<Cart> cartList = new ArrayList<>();

    CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        compositeDisposable = new CompositeDisposable();

        firebaseFirestore = FirebaseFirestore.getInstance();

        mService = Common.getAPI();
        mServiceScalars = Common.getScalarsAPI();

        cartRecycler = findViewById(R.id.favorite_recycler);
        cartRecycler.setLayoutManager(new LinearLayoutManager(this));
        cartRecycler.setHasFixedSize(true);

        ItemTouchHelper.SimpleCallback simpleCallback = new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(cartRecycler);

        placeOrderButton = findViewById(R.id.place_order_button);
        placeOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });

        rootLayout = findViewById(R.id.rootLayout);

        loadCartItems();

        loadToken();
    }

    private void loadToken() {

        final AlertDialog waitingDialog = new SpotsDialog(CartActivity.this);
        waitingDialog.show();
        waitingDialog.setMessage("Por favor espere...");

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Common.API_TOKEN_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                waitingDialog.dismiss();
                placeOrderButton.setEnabled(false);
                Toast.makeText(CartActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                waitingDialog.dismiss();

                token = responseString;
                placeOrderButton.setEnabled(true);
            }
        });
    }

    private void placeOrder() {

        if(Common.currentUser != null) {
            //Create dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Submit Order");

            View submit_order_layout = LayoutInflater.from(this).inflate(R.layout.submit_order_layout, null);

            final EditText commentEditText = submit_order_layout.findViewById(R.id.comment_edit_text);
            final EditText otherAddressEditText = submit_order_layout.findViewById(R.id.other_address_edit_text);

            otherAddressEditText.setText(Common.currentUser.getAddress());

            final RadioButton userAddressRadioButton = submit_order_layout.findViewById(R.id.user_address_radio_button);
            final RadioButton otherAddressRadioButton = submit_order_layout.findViewById(R.id.other_address_radio_button);

            //Event
            userAddressRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        otherAddressEditText.setEnabled(false);
                        otherAddressEditText.setText(Common.currentUser.getAddress());
                    }
                }
            });
            otherAddressRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        otherAddressEditText.setEnabled(true);
                        otherAddressEditText.setText("");
                    }
                }
            });

            builder.setView(submit_order_layout);

            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    final String orderComment = commentEditText.getText().toString();
                    final String orderAddress;
                    if (userAddressRadioButton.isChecked())
                        orderAddress = Common.currentUser.getAddress();
                    else if (otherAddressRadioButton.isChecked())
                        orderAddress = otherAddressEditText.getText().toString();
                    else
                        orderAddress = "";

                    //Payment
                    DropInRequest dropInRequest = new DropInRequest().clientToken(token);
                    startActivityForResult(dropInRequest.getIntent(CartActivity.this), PAYMENT_REQUEST_CODE);

                    //Todo.

                    //Submit order
                    compositeDisposable.add(
                            Common.cartRepository.getCartItems()
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(new Consumer<List<Cart>>() {
                                        @Override
                                        public void accept(List<Cart> carts) throws Exception {
                                            if (!TextUtils.isEmpty(orderAddress))
                                                sendOrderToServer(Common.cartRepository.sumPrice(), carts, orderComment, orderAddress);
                                            else
                                                Toast.makeText(CartActivity.this, "Debe tener direccion de envio", Toast.LENGTH_SHORT).show();
                                        }
                                    })
                    );
                }
            });

            builder.show();

        } else {

            //Require login
            AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
            builder.setTitle("NO REGISTRADO?");
            builder.setMessage("Por Favor ingresar con tu cuenta o registrate para hacer la compra");
            builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    startActivity(new Intent(CartActivity.this, MainActivity.class));
                    finish();
                }
            }).show();
        }
    }

    //Ctrl+O
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == PAYMENT_REQUEST_CODE){

            if(resultCode == RESULT_OK){

                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                PaymentMethodNonce nonce = result.getPaymentMethodNonce();
                String strNonce = nonce.getNonce();

                if(Common.cartRepository.sumPrice() > 0){

                    amount = String.valueOf(Common.cartRepository.sumPrice());
                    params = new HashMap<>();

                    params.put("amount",amount);
                    params.put("nonce",strNonce);

                    sendPayment();

                } else {
                    Toast.makeText(this, "Cantidad del pago es 0", Toast.LENGTH_SHORT).show();
                }

            } else if(resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Pago cancelado", Toast.LENGTH_SHORT).show();

            } else {
                Exception error = (Exception)data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Log.e(" onActivityResult ERROR",  error.getMessage());
            }
        }
    }

    private void sendPayment() {

        mServiceScalars.payment(params.get("nonce"),params.get("amount"))
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if(response.body().toString().contains("Successful")) {

                            Toast.makeText(CartActivity.this, "Transaccion realizada", Toast.LENGTH_SHORT).show();
                            //Submit order
                            compositeDisposable.add(
                                    Common.cartRepository.getCartItems()
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribeOn(Schedulers.io())
                                            .subscribe(new Consumer<List<Cart>>() {
                                                @Override
                                                public void accept(List<Cart> carts) throws Exception {
                                                    if(!TextUtils.isEmpty(orderAddress))
                                                        sendOrderToServer(Common.cartRepository.sumPrice(),carts,orderComment,orderAddress);
                                                    else
                                                        Toast.makeText(CartActivity.this, "La direccion no puede estar vacia", Toast.LENGTH_SHORT).show();
                                                }
                                            })
                            );

                        } else {

                            Toast.makeText(CartActivity.this, "Error en la transaccion!", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e(" onFailure ERROR",  t.getMessage());
                    }
                });

    }


    private void sendOrderToServer(float sumPrice, List<Cart> carts, String orderComment, String orderAddress) {

        if(carts.size() > 0){
            String orderDetail = new Gson().toJson(carts);

            //INSERT NEW ORDER (to Order collection)

            final Map<String, Object> order = new HashMap<>();
            order.put("orderDate", FieldValue.serverTimestamp());
            order.put("orderStatus", 0);
            order.put("orderPrice", sumPrice);
            order.put("orderDetail", orderDetail);
            order.put("orderAddress", orderAddress);
            order.put("orderComment", orderComment);
            order.put("userPhone", Common.currentUser.getPhone());

            //for pass correct Order object in sendNotificationToSercer method
            //Order order1 = new Order(0,FieldValue.serverTimestamp(),sumPrice,orderDetail,orderComment,orderAddress,Common.currentUser.getPhone());

            firebaseFirestore.collection("Orders")
                    .add(order)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Toast.makeText(CartActivity.this, "Cart seteado a server", Toast.LENGTH_SHORT).show();
                            //clear cart
                            Common.cartRepository.emptyCart();
                            finish();
                            sendNotificationToServer(Common.currentUser.getPhone()); //Todo. Cuando tenga listo el DrinkShopFirestoreServer
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(TAG, "onFailure: ERROR EN REGISTRO " + e.getMessage());
                            Toast.makeText(CartActivity.this, "Error en ingresar cart", Toast.LENGTH_SHORT).show();
                        }
                    });

//            mService.submitOrder(sumPrice,orderDetail,orderComment,orderAddress,Common.currentUser.getPhone())
//                    .enqueue(new Callback<OrderResult>() {
//                        @Override
//                        public void onResponse(Call<OrderResult> call, Response<OrderResult> response) {
//                            sendNotificationToServer(order);
//                        }
//
//                        @Override
//                        public void onFailure(Call<OrderResult> call, Throwable t) {
//                            Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    });

        }
    }

    private void sendNotificationToServer(final String userPhone) {
        //Get Server token (server_app_01 is phone put by us by Server app)

        //Get token from Firestore!!

        firebaseFirestore.collection("Tokens").document("server_app_01")
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            Token token = document.toObject(Token.class);
                            String userToken = token.getToken();

                            //
                            Map<String,String> contentSend = new HashMap<>();
                            contentSend.put("title","NICODev");
                            contentSend.put("message", "Tienes una nueva orden de : " + userPhone);
                            DataMessage dataMessage = new DataMessage();
                            //Toast.makeText(CartActivity.this, "TOEKN; " + response.body().getToken(), Toast.LENGTH_SHORT).show();
                            dataMessage.setTo(userToken);
                            dataMessage.setData(contentSend);

                            IFCMService ifcmService = Common.getFCMService();
                            ifcmService.sendNotification(dataMessage)
                                    .enqueue(new Callback<MyResponse>() {
                                        @Override
                                        public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                            if(response.code() == 200){
                                                if(response.body().success == 1){
                                                    Toast.makeText(CartActivity.this, "Gracias, Orden Enviada!", Toast.LENGTH_SHORT).show();
                                                    //clear cart
                                                    Common.cartRepository.emptyCart();
                                                    finish();
                                                } else {
                                                    Toast.makeText(CartActivity.this, "Error en envio de orden", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<MyResponse> call, Throwable t) {
                                            Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });

                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });

//        mService.getToken("server_app_01","1")
//                .enqueue(new Callback<Token>() {
//                    @Override
//                    public void onResponse(Call<Token> call, Response<Token> response) {
//                        //When we have token , just send notification to this token
//                        Map<String,String> contentSend = new HashMap<>();
//                        contentSend.put("title","NICODev");
//                        contentSend.put("message", "Tienes una nueva orden: " + orderResult.getOrderId());
//                        DataMessage dataMessage = new DataMessage();
//                        //Toast.makeText(CartActivity.this, "TOEKN; " + response.body().getToken(), Toast.LENGTH_SHORT).show();
//                        if(response.body().getToken() != null)
//                            dataMessage.setTo(response.body().getToken());
//                        dataMessage.setData(contentSend);
//
//                        IFCMService ifcmService = Common.getFCMService();
//                        ifcmService.sendNotification(dataMessage)
//                                .enqueue(new Callback<MyResponse>() {
//                                    @Override
//                                    public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
//                                        if(response.code() == 200){
//                                            if(response.body().success == 1){
//                                                Toast.makeText(CartActivity.this, "Gracias, Orden Enviada!", Toast.LENGTH_SHORT).show();
//                                                //clear cart
//                                                Common.cartRepository.emptyCart();
//                                                finish();
//                                            } else {
//                                                Toast.makeText(CartActivity.this, "Error en envio de orden", Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<MyResponse> call, Throwable t) {
//                                        Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                    }
//
//                    @Override
//                    public void onFailure(Call<Token> call, Throwable t) {
//                        Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });


    }


    private void loadCartItems() {
        compositeDisposable.add(
                Common.cartRepository.getCartItems()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Cart>>() {
                    @Override
                    public void accept(List<Cart> carts) throws Exception {
                        displayCartItem(carts);
                    }
                })
        );
    }

    private void displayCartItem(List<Cart> carts) {
        cartList = carts;
        cartAdapter = new CartAdapter(this,carts);
        cartRecycler.setAdapter(cartAdapter);
    }

    //ctrl+O

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadCartItems();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if(viewHolder instanceof CartAdapter.CartViewHolder){

            String name = cartList.get(viewHolder.getAdapterPosition()).name;
            final Cart deletedItem = cartList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            //Delete item from adapter
            cartAdapter.removeItem(deletedIndex);
            //Delete item from Room database
            Common.cartRepository.deleteCartItems(deletedItem);

            Snackbar snackbar = Snackbar.make(rootLayout, new StringBuilder(name).append(" removida de la lista de compra").toString(),
                    Snackbar.LENGTH_LONG);
            snackbar.setAction("DESHACER", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartAdapter.restoreItem(deletedItem,deletedIndex);
                    Common.cartRepository.insertToCart(deletedItem);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();

        }
    }
}
