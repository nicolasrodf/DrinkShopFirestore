package com.nicolasrf.drinkshopfirestore;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.nicolasrf.drinkshopfirestore.Model.CheckUserResponse;
import com.nicolasrf.drinkshopfirestore.Model.Drink;
import com.nicolasrf.drinkshopfirestore.Model.Token;
import com.nicolasrf.drinkshopfirestore.Model.User;
import com.nicolasrf.drinkshopfirestore.Retrofit.IDrinkShopAPI;
import com.nicolasrf.drinkshopfirestore.Utils.Common;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int REQUEST_CODE = 1000;
    private static final int REQUEST_PERMISSION = 1001;
    Button continueButton;

    IDrinkShopAPI mService;


    FirebaseAuth auth;

    private FirebaseFirestore firebaseFirestore;

    private final int REQUEST_LOGIN = 2000;

    //Ctrl+O


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case REQUEST_PERMISSION:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Permiso concedido", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
                }
            }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        auth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();


        continueButton = findViewById(R.id.continue_button);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSession();
            }
        });




//
//        firebaseFirestore = FirebaseFirestore.getInstance();
//
//        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this, new String[]{
//                    Manifest.permission.READ_EXTERNAL_STORAGE
//            }, REQUEST_PERMISSION);
//        }
//
//        mService = Common.getAPI();
//
//        continueButton = findViewById(R.id.continue_button);
//        continueButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startLoginPage(LoginType.PHONE);
//            }
//        });
//
//


    }

    private void checkSession() {

        //Check session

        if (auth.getCurrentUser() != null) {

            final AlertDialog alertDialog = new SpotsDialog(MainActivity.this);
            alertDialog.show();
            alertDialog.setMessage("Por favor espere...");
            //Auto login

            //if already login
            if (!FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().isEmpty()) {
//                    startActivity(new Intent(this,SignInActivity.class)
//                            .putExtra("phone", FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber()));
//                    finish();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                Toast.makeText(MainActivity.this, "USER PHONE " +
                        user.getPhoneNumber(), Toast.LENGTH_SHORT).show();

                //check if exists on database

                //showRegisterDialog(user.getPhoneNumber());
                DocumentReference docRef = firebaseFirestore.collection("Users")
                        .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                alertDialog.dismiss();
                                Common.currentUser = document.toObject(User.class);
                                //update token
                                updateTokenToServer();
                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                finish(); //Close MainActivity
                            } else {
                                Log.d(TAG, "No such document");
                                //need register
                                alertDialog.dismiss();
                                Log.d(TAG, "onResponse: USER NOT EXISTS");
                                showRegisterDialog(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: FALLA EN USER CHECK . " + e.getMessage());
                        showRegisterDialog(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                    }
                });


            } else {
                startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder().setAvailableProviders(
                                Arrays.asList(new AuthUI.IdpConfig.PhoneBuilder().build())).build(), REQUEST_LOGIN);
            }
        } else {

            startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder().setAvailableProviders(
                            Arrays.asList(new AuthUI.IdpConfig.PhoneBuilder().build())).build(), REQUEST_LOGIN);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_LOGIN){

            IdpResponse response = IdpResponse.fromResultIntent(data);

            //Succesffuly signed in
            if(resultCode == RESULT_OK){

                if(!FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().isEmpty()) {
//                    startActivity(new Intent(this,SignInActivity.class)
//                            .putExtra("phone",FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber()));
//                    finish();
//                    return;

                    final AlertDialog alertDialog = new SpotsDialog(MainActivity.this);
                    alertDialog.show();
                    alertDialog.setMessage("Please waiting...");

                    //check user exists
                    DocumentReference docRef = firebaseFirestore.collection("Users")
                            .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                    docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                    alertDialog.dismiss();
                                    Common.currentUser = document.toObject(User.class);
                                    //update token
                                    updateTokenToServer();
                                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                    finish(); //Close MainActivity
                                } else {
                                    Log.d(TAG, "No such document");
                                    //need register
                                    alertDialog.dismiss();
                                    Log.d(TAG, "onResponse: USER NOT EXISTS");
                                    showRegisterDialog(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                                }
                            } else {
                                Log.d(TAG, "get failed with ", task.getException());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(TAG, "onFailure: FALLA EN USER CHECK . " + e.getMessage());
                            showRegisterDialog(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
                        }
                    });

                    Toast.makeText(MainActivity.this, "USER PHONE " + FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber(), Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();

                }


                } else {
                    if(response == null) {
                        Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                        Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(response.getError().getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                        Toast.makeText(this, "Unknown Error", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                Toast.makeText(this, "UNksnown Sign In error!", Toast.LENGTH_SHORT).show();
            }


    }

    private void showRegisterDialog(final String phone){
        Log.d(TAG, "showRegisterDialog: STARTED");

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("REGISTRAR");

        LayoutInflater inflater = this.getLayoutInflater();
        View register_layout = inflater.inflate(R.layout.register_layout, null);

        final MaterialEditText nameEditText = register_layout.findViewById(R.id.name_edit_text);
        final MaterialEditText addressEditText = register_layout.findViewById(R.id.address_edit_text);
        final MaterialEditText birthdateEditText = register_layout.findViewById(R.id.birthdate_edit_text);

        Button registerButton = register_layout.findViewById(R.id.register_button);

        birthdateEditText.addTextChangedListener(new PatternedTextWatcher("####-##-##"));

        builder.setView(register_layout);
        final AlertDialog dialog = builder.create();

        //Event
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if(TextUtils.isEmpty(addressEditText.getText().toString())){
                    Toast.makeText(MainActivity.this, "Por favor ingresa tu direccion", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(birthdateEditText.getText().toString())){
                    Toast.makeText(MainActivity.this, "Por favor ingresa tu fecha de cumpleaños", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(nameEditText.getText().toString())){
                    Toast.makeText(MainActivity.this, "Por favor ingresa tu nombre", Toast.LENGTH_SHORT).show();
                    return;
                }

                final AlertDialog waitingDialog = new SpotsDialog(MainActivity.this);
                waitingDialog.show();
                waitingDialog.setMessage("Please waiting...");


                //REIGSTER NEW USER (to User collection)

                final User user = new User(phone,addressEditText.getText().toString(),
                        nameEditText.getText().toString(),birthdateEditText.getText().toString(),"");

//                Map<String,Object> user = new HashMap<>();
//                user.put("name", nameEditText.getText().toString());
//                user.put("phone", phone);
//                user.put("address", addressEditText.getText().toString());
//                user.put("birthdate", birthdateEditText.getText().toString());
//                user.put("avatarUrl", "");

                firebaseFirestore.collection("Users")
                        .document(phone)
                        .set(user)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(MainActivity.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                                Common.currentUser = user;
                                waitingDialog.dismiss();
                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                finish(); //Close MainActivity
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "onFailure: ERROR EN REGISTRO " + e.getMessage());
                                Toast.makeText(MainActivity.this, "Error en registro", Toast.LENGTH_SHORT).show();
                                waitingDialog.dismiss();
                            }
                });

                updateTokenToServer();

                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                finish();//cloase MainActivity

            }

        });

        dialog.show();
    }

    private void printKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.nicolasrf.orderfoodapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    //Exit App when click Back button
    boolean isBackButtonClicked = false;
    //Ctrl+O
    @Override
    public void onBackPressed() {
        if(isBackButtonClicked){
            super.onBackPressed();
            return;
        }
        this.isBackButtonClicked = true;
        Toast.makeText(this, "Por favor presionar ATRAS para salir de la aplicacion", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        isBackButtonClicked = false;
        super.onResume();
    }

    private void updateTokenToServer() {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {

                        //craate or update token collection

                        Token token = new Token(Common.currentUser.getPhone(),instanceIdResult.getToken(),"0");
                        firebaseFirestore.collection("Tokens").document(Common.currentUser.getPhone()).set(token)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(MainActivity.this, "token exitoso", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "onFailure: ERROR EN MANEJO DE TOKEN " + e.getMessage());
                                Toast.makeText(MainActivity.this, "Error en ingreso de token", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
