package com.nicolasrf.drinkshopfirestore.Database.Local;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Cart;
import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Favorite;

/**
 * Created by Nicolas on 23/05/2018.
 */

@Database(entities = {Cart.class, Favorite.class}, version = 2, exportSchema = false)
public abstract class NICORoomDatabase extends RoomDatabase{
    public abstract CartDAO cartDAO();
    public abstract  FavoriteDAO favoriteDAO();

    private static NICORoomDatabase instance;

    public static NICORoomDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context,NICORoomDatabase.class,"NICO_DrinkShopDB")
                    .allowMainThreadQueries()
                    //.addMigrations(MIGRATION_1_2)
                    .build();
        }
        return instance;
    }

//    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            // Since we didn't alter the table, there's nothing else to do here.
//        }
//    };

}
