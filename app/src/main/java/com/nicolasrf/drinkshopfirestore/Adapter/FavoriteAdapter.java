package com.nicolasrf.drinkshopfirestore.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.gson.Gson;
import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Cart;
import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Favorite;
import com.nicolasrf.drinkshopfirestore.Interface.IItemClickListener;
import com.nicolasrf.drinkshopfirestore.R;
import com.nicolasrf.drinkshopfirestore.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Tiene el ViewHolder dentro del mismo Adapter, no por separado como Card, Category, y Drink !
 */

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {
    private static final String TAG = "FavoriteAdapter";

    Context context;
    List<Favorite> favoriteList;

    public FavoriteAdapter(Context context, List<Favorite> favoriteList) {
        this.context = context;
        this.favoriteList = favoriteList;
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fav_item_layout, parent,false);
        return new FavoriteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteViewHolder holder, final int position) {
        Picasso.with(context)
                .load(favoriteList.get(position).link)
                .into(holder.productImageView);
        holder.priceTextView.setText(new StringBuilder("$").append(favoriteList.get(position).price).toString());
        holder.productNameTextView.setText(favoriteList.get(position).name);

        holder.addToCartImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddToCartDialog(position);
            }
        });

        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                //Todo. Que se vaya a una venta con info de foto y descripcion!
                Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.drink_detail_layout);
                dialog.setTitle("My Custom Dialog");

                ImageView imageView = dialog.findViewById(R.id.favorite_image);
                Picasso.with(context)
                        .load(favoriteList.get(position).link)
                        .into(imageView);
                TextView textView = dialog.findViewById(R.id.descTextView);
                textView.setText(favoriteList.get(position).description); //AQUI FALTA UN DESCRIPTION

                dialog.show();
            }
        });

    }


    private void showAddToCartDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View itemView = LayoutInflater.from(context).inflate(R.layout.add_to_cart_layout,null);

        //View
        ImageView productDialogImageView = itemView.findViewById(R.id.product_cart_image_view);
        final ElegantNumberButton countText = itemView.findViewById(R.id.count_text_number);
        TextView productDialogTextView = itemView.findViewById(R.id.product_cart_name_text_view);

        EditText commentEditText = itemView.findViewById(R.id.comment_edit_text);

        RadioButton sizeMRadioButton = itemView.findViewById(R.id.sizeM_radio_button);
        RadioButton sizeLRadioButton = itemView.findViewById(R.id.sizeL_radio_button);

        sizeMRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sizeOfCup=0;
                }
            }
        });
        sizeLRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sizeOfCup=1;
                }
            }
        });

        RadioButton sugar100RadioButton = itemView.findViewById(R.id.sugar100_radio_button);
        RadioButton sugar70RadioButton = itemView.findViewById(R.id.sugar70_radio_button);
        RadioButton sugar50RadioButton = itemView.findViewById(R.id.sugar50_radio_button);
        RadioButton sugar30RadioButton = itemView.findViewById(R.id.sugar30_radio_button);
        RadioButton sugarFreeRadioButton = itemView.findViewById(R.id.sugarFree_radio_button);

        sugar30RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sugar=30;
                }
            }
        });
        sugar50RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sugar=50;
                }
            }
        });
        sugar70RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sugar=70;
                }
            }
        });
        sugar100RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sugar=100;
                }
            }
        });
        sugarFreeRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.sugar=0;
                }
            }
        });

        RadioButton ice100RadioButton = itemView.findViewById(R.id.ice100_radio_button);
        RadioButton ice70RadioButton = itemView.findViewById(R.id.ice70_radio_button);
        RadioButton ice50RadioButton = itemView.findViewById(R.id.ice50_radio_button);
        RadioButton ice30RadioButton = itemView.findViewById(R.id.ice30_radio_button);
        RadioButton iceFreeRadioButton = itemView.findViewById(R.id.iceFree_radio_button);

        ice30RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.ice=30;
                }
            }
        });
        ice50RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.ice=50;
                }
            }
        });
        ice70RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.ice=70;
                }
            }
        });
        ice100RadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.ice=100;
                }
            }
        });
        iceFreeRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Common.ice=0;
                }
            }
        });


        RecyclerView toppingRecycler = itemView.findViewById(R.id.topping_recycler);
        toppingRecycler.setLayoutManager(new LinearLayoutManager(context));
        toppingRecycler.setHasFixedSize(true);

        MultiChoiceAdapter adapter = new MultiChoiceAdapter(context, Common.toppingList);
        toppingRecycler.setAdapter(adapter);

        //Set data
        Picasso.with(context)
                .load(favoriteList.get(position).link)
                .into(productDialogImageView);
        productDialogTextView.setText(favoriteList.get(position).name);

        //Set negative (add to cart) button
        builder.setView(itemView);
        builder.setNegativeButton("AGREGAR AL CARRITO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(Common.sizeOfCup == -1){
                    Toast.makeText(context, "Por favor selecciona tamaño de vaso", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Common.sugar == -1){
                    Toast.makeText(context, "Por favor elige azucar", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Common.ice == -1){
                    Toast.makeText(context, "Por favor elige hielo", Toast.LENGTH_SHORT).show();
                    return;
                }
                showConfirmDialog(position,countText.getNumber());
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void showConfirmDialog(final int position, final String number) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View itemView = LayoutInflater.from(context).inflate(R.layout.confirm_add_to_cart_layout,null);

        //View
        ImageView productDialogImageView = itemView.findViewById(R.id.product_image_view);
        final TextView productDialogTextView = itemView.findViewById(R.id.product_cart_name_text_view);
        final TextView productPriceTextView = itemView.findViewById(R.id.product_cart_price_text_view);
        TextView sugarTextView = itemView.findViewById(R.id.sugar_text_view);
        TextView iceTextView = itemView.findViewById(R.id.ice_text_view);
        final TextView toppingExtraTextView = itemView.findViewById(R.id.topping_extra_text_view);

        //Set data
        Picasso.with(context)
                .load(favoriteList.get(position).link)
                .into(productDialogImageView);
        productDialogTextView.setText(new StringBuilder(favoriteList.get(position).name).append(" x")
                .append(Common.sizeOfCup == 0 ? " Tamaño M":" Tamaño L")
                .append(number).toString());

        iceTextView.setText(new StringBuilder("Hielo: ").append(Common.ice).append("%").toString());
        sugarTextView.setText(new StringBuilder("Azucar: ").append(Common.sugar).append("%").toString());

        double price = (favoriteList.get(position).price*Double.parseDouble(number)) + Common.toppingPrice;

        if(Common.sizeOfCup == 1){ //Size L
            price+=(3.0*Double.parseDouble(number));
        }
        StringBuilder topping_final_comment = new StringBuilder("");

        for(String line:Common.toppingAdded){

            topping_final_comment.append(line).append("\n");

            toppingExtraTextView.setText(topping_final_comment);

            final double finalPrice = Math.round(price);

            productPriceTextView.setText(new StringBuilder("$").append(finalPrice));

            builder.setNegativeButton("CONFIRMAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //Add to SQlite
                    //Create new cart item
                    try {
                        Cart cartItem = new Cart();
                        cartItem.name = favoriteList.get(position).name;
                        cartItem.amount = Integer.parseInt(number);
                        cartItem.ice = Common.ice;
                        cartItem.sugar = Common.sugar;
                        cartItem.price = finalPrice;
                        cartItem.size = Common.sizeOfCup;
                        cartItem.toppingExtras = toppingExtraTextView.getText().toString();
                        cartItem.link = favoriteList.get(position).link;

                        //Add to DB
                        Common.cartRepository.insertToCart(cartItem);

                        Log.d(TAG, "NICO_DEBUG:" + new Gson().toJson(cartItem));
                        Toast.makeText(context, "Agregado al carrito de compras", Toast.LENGTH_SHORT).show();

                    } catch (NumberFormatException e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        builder.setView(itemView);
        builder.show();


    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }

    public void removeItem(int position){
        favoriteList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Favorite item, int position){
        favoriteList.add(position,item);
        notifyItemInserted(position);
    }

    //ViewHolder

    public class FavoriteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private IItemClickListener itemClickListener;

        public void setItemClickListener(IItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        ImageView productImageView, addToCartImageView;
        TextView productNameTextView, priceTextView;

        public RelativeLayout backgroundRelativaLayout;
        public LinearLayout foregroundLinearLayout;

        public FavoriteViewHolder(View itemView) {
            super(itemView);

            productImageView = itemView.findViewById(R.id.product_image_view);
            productNameTextView = itemView.findViewById(R.id.product_name_text_view);
            priceTextView = itemView.findViewById(R.id.price_text_view);
            addToCartImageView = itemView.findViewById(R.id.add_cart_button);

            backgroundRelativaLayout = itemView.findViewById(R.id.background_relative_layout);
            foregroundLinearLayout = itemView.findViewById(R.id.foreground_linear_layout);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v);
        }

    }
}
