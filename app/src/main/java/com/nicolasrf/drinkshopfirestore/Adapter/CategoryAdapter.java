package com.nicolasrf.drinkshopfirestore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nicolasrf.drinkshopfirestore.DrinkActivity;
import com.nicolasrf.drinkshopfirestore.Interface.IItemClickListener;
import com.nicolasrf.drinkshopfirestore.Model.Category;
import com.nicolasrf.drinkshopfirestore.R;
import com.nicolasrf.drinkshopfirestore.Utils.Common;
import com.nicolasrf.drinkshopfirestore.ViewHolder.CategoryViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Nicolas on 18/05/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private Context context;
    private List<Category> categories;

    public CategoryAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.menu_item_layout,null);

        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {

        final String menuId = categories.get(position).menuId;

        Picasso.with(context)
                .load(categories.get(position).link)
                .into(holder.productImageView);

        holder.menuNameTextView.setText(categories.get(position).name);

        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onClick(View v) {
                Common.currentCategory = categories.get(position);
                //Toast.makeText(context, "CATEGORY MENU ID " + menuId, Toast.LENGTH_SHORT).show();

                Intent commentIntent = new Intent(context, DrinkActivity.class);
                commentIntent.putExtra("menu_id", menuId);
                context.startActivity(commentIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


}
