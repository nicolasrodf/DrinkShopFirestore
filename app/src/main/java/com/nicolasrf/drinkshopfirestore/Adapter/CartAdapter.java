package com.nicolasrf.drinkshopfirestore.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Cart;
import com.nicolasrf.drinkshopfirestore.Database.ModelDB.Favorite;
import com.nicolasrf.drinkshopfirestore.R;
import com.nicolasrf.drinkshopfirestore.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Nicolas on 23/05/2018.
 */

public class CartAdapter extends  RecyclerView.Adapter<CartAdapter.CartViewHolder>{

    Context context;
    List<Cart> cartList;

    public CartAdapter(Context context, List<Cart> cartList) {
        this.context = context;
        this.cartList = cartList;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.cart_item_layout,parent,false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartViewHolder holder, final int position) {
        Picasso.with(context)
                .load(cartList.get(position).link)
                .into(holder.productImageView);

        holder.amountText.setNumber(String.valueOf(cartList.get(position).amount));
        holder.priceTextView.setText(new StringBuilder("$").append(cartList.get(position).price));
        holder.productNameTextView.setText(new StringBuilder(cartList.get(position).name)
                .append(" x")
                .append(cartList.get(position).amount)
                .append(cartList.get(position).size == 0 ? " Tamaño M":"Tamaño L"));
        holder.sugarIceTextView.setText(new StringBuilder("Azucar: ")
                .append(cartList.get(position).sugar).append("%").append("\n")
                .append("Ice: ").append(cartList.get(position).ice)
                .append("%").toString());

        //get price of one cup with all options
        final double priceOneCup = cartList.get(position).price / cartList.get(position).amount;

        //Auto save item when user change amount
        holder.amountText.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Cart cart = cartList.get(position);
                cart.amount = newValue;
                cart.price = Math.round(priceOneCup*newValue);

                Common.cartRepository.updateCart(cart);

                holder.priceTextView.setText(new StringBuilder("$").append(cartList.get(position).price));
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder{

        ImageView productImageView;
        TextView productNameTextView, sugarIceTextView, priceTextView;
        ElegantNumberButton amountText;

        public RelativeLayout backgroundRelativeLayout;
        public LinearLayout foregroundLinearLayout;

        public CartViewHolder(View itemView) {
            super(itemView);

            productImageView = itemView.findViewById(R.id.product_image_view);
            productNameTextView = itemView.findViewById(R.id.product_name_text_view);
            sugarIceTextView = itemView.findViewById(R.id.sugar_ice_text_view);
            priceTextView = itemView.findViewById(R.id.price_text_view);
            amountText = itemView.findViewById(R.id.amount_text);

            backgroundRelativeLayout = itemView.findViewById(R.id.background_relative_layout);
            foregroundLinearLayout = itemView.findViewById(R.id.foreground_linear_layout);
        }
    }

    public void removeItem(int position){
        cartList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Cart item, int position){
        cartList.add(position,item);
        notifyItemInserted(position);
    }
}
