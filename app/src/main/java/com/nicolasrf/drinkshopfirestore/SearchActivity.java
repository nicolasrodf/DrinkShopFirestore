package com.nicolasrf.drinkshopfirestore;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.nicolasrf.drinkshopfirestore.Adapter.DrinkAdapter;
import com.nicolasrf.drinkshopfirestore.Model.Drink;
import com.nicolasrf.drinkshopfirestore.Retrofit.IDrinkShopAPI;
import com.nicolasrf.drinkshopfirestore.Utils.Common;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";

    List<String> suggestList = new ArrayList<>();
    List<Drink> localDataSource = new ArrayList<>();
    MaterialSearchBar searchBar;

    private FirebaseFirestore firebaseFirestore;

    IDrinkShopAPI mService;
    RecyclerView search_recycler;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    DrinkAdapter searchAdapter, adapter;

    int menuSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        firebaseFirestore = FirebaseFirestore.getInstance();

        //Init service
        mService = Common.getAPI();

        search_recycler = findViewById(R.id.search_recycler);
        search_recycler.setLayoutManager(new GridLayoutManager(this,2));

        searchBar = findViewById(R.id.searchBar);
        searchBar.setHint("Ingresa tu bebida");

        getMenuSizeAndLoadDrinks();

        searchBar.setCardViewElevation(10);
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                List<String> suggest = new ArrayList<>();
                for(String search:suggestList){
                    if(search.toLowerCase().contains(searchBar.getText().toLowerCase()))
                        suggest.add(search);
                }
                searchBar.setLastSuggestions(suggest);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                if(!enabled)
                    search_recycler.setAdapter(adapter);// restore full list of drink
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                startSearch(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });

    }

    private void getMenuSizeAndLoadDrinks() {

        firebaseFirestore.collection("Menu")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                int menuSize = task.getResult().size();
                                loadAllDrinks(menuSize);
                            }

                        }

                    }

                });

    }

    private void startSearch(CharSequence text) {
        List<Drink> result = new ArrayList<>();
        for(Drink drink:localDataSource) {
            if (drink.name.contains(text))
                result.add(drink);
        }
        searchAdapter = new DrinkAdapter(this,result);
        search_recycler.setAdapter(searchAdapter);
    }

    //Ctrl+O
    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }


    private void loadAllDrinks(int menuSize) {

        final List<Drink> drinks = new ArrayList<>();

        for(int i=1; i<=menuSize; i++) {

            firebaseFirestore.collection("Menu/" + i + "/Drinks")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());

                                    //Toast.makeText(HomeActivity.this, "name: " + document.get("name"), Toast.LENGTH_SHORT).show();
                                    Drink drink = document.toObject(Drink.class).withId(document.getId());
                                    drinks.add(drink);

                                }

                                displayDrinkList(drinks);
                                buildSuggestList(drinks);
                            }

                        }

                    });
        }

    }

    private void getDrinksSize(List<Drink> drinks) {
        Toast.makeText(this, "DRINKS: " + drinks.size(), Toast.LENGTH_SHORT).show();
    }

    private void buildSuggestList(List<Drink> drinks) {
        for(Drink drink:drinks)
            suggestList.add(drink.name);
        searchBar.setLastSuggestions(suggestList);
    }

    private void displayDrinkList(List<Drink> drinks) {
        localDataSource = drinks;
        adapter = new DrinkAdapter(this,drinks);
        search_recycler.setAdapter(adapter);
    }
}
