package com.nicolasrf.drinkshopfirestore.Model;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

/**
 * Created by Nicolas on 18/05/2018.
 */

public class Category{

    public String name;
    public String link;

    @Exclude
    public String menuId;

    public Category() {
    }

    public Category(String name, String link) {
        this.name = name;
        this.link = link;
    }

    public <T extends Category> T withId(@NonNull final String id) {
        this.menuId = id;
        return (T) this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
