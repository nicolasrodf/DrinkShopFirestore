package com.nicolasrf.drinkshopfirestore.Model;

/**
 * Created by Nicolas on 17/05/2018.
 */

public class Banner {
    private String name;
    private String link;

    public Banner() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
