package com.nicolasrf.drinkshopfirestore.Model;

/**
 * Created by Nicolas on 16/05/2018.
 */

public class User {

    private String phone;
    private String address;
    private String name;
    private String birthdate;
    private String avatarUrl;

    public User() {
    }

    public User(String phone, String address, String name, String birthdate, String avatarUrl) {
        this.phone = phone;
        this.address = address;
        this.name = name;
        this.birthdate = birthdate;
        this.avatarUrl = avatarUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }


    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
