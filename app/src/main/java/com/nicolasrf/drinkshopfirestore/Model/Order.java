package com.nicolasrf.drinkshopfirestore.Model;

import android.support.annotation.NonNull;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Order {
    private int orderStatus;
    @ServerTimestamp private Date orderDate;
    private float orderPrice;
    private String orderDetail, orderComment, orderAddress, userPhone;

    @Exclude
    public String orderId;

    public Order() {
    }

    public Order(int orderStatus, Date orderDate, float orderPrice, String orderDetail, String orderComment, String orderAddress, String userPhone) {
        this.orderStatus = orderStatus;
        this.orderDate = orderDate;
        this.orderPrice = orderPrice;
        this.orderDetail = orderDetail;
        this.orderComment = orderComment;
        this.orderAddress = orderAddress;
        this.userPhone = userPhone;
    }

    public <T extends Order> T withId(@NonNull final String id) {
        this.orderId = id;
        return (T) this;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public float getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(float orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(String orderDetail) {
        this.orderDetail = orderDetail;
    }

    public String getOrderComment() {
        return orderComment;
    }

    public void setOrderComment(String orderComment) {
        this.orderComment = orderComment;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(String orderAddress) {
        this.orderAddress = orderAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
