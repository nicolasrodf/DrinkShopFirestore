package com.nicolasrf.drinkshopfirestore.Model;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

/**
 * Created by Nicolas on 20/05/2018.
 */

public class Drink {

    public String name;
    public String link;
    public float price;
    public String description;

    public Drink() {
    }

    public Drink(String name, String link, float price, String description) {
        this.name = name;
        this.link = link;
        this.price = price;
        this.description = description;
    }

    @Exclude
    public String drinkId;

    public <T extends Drink> T withId(@NonNull final String id) {
        this.drinkId = id;
        return (T) this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(String drinkId) {
        this.drinkId = drinkId;
    }
}
