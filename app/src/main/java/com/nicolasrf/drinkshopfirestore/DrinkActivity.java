package com.nicolasrf.drinkshopfirestore;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.nicolasrf.drinkshopfirestore.Adapter.DrinkAdapter;
import com.nicolasrf.drinkshopfirestore.Model.Category;
import com.nicolasrf.drinkshopfirestore.Model.Drink;
import com.nicolasrf.drinkshopfirestore.Retrofit.IDrinkShopAPI;
import com.nicolasrf.drinkshopfirestore.Utils.Common;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class DrinkActivity extends AppCompatActivity {
    private static final String TAG = "DrinkActivity";

    IDrinkShopAPI mService;
    RecyclerView drinkRecycler;

    private FirebaseFirestore firebaseFirestore;

    TextView bannerTextView;

    SwipeRefreshLayout refreshLayout;

    //RxJava
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        mService = Common.getAPI();

        firebaseFirestore = FirebaseFirestore.getInstance();

        refreshLayout = findViewById(R.id.swipe_layout);

        drinkRecycler = findViewById(R.id.recycler_drinks);
        drinkRecycler.setLayoutManager(new GridLayoutManager(this,2));
        drinkRecycler.setHasFixedSize(true);

        bannerTextView = findViewById(R.id.name_text_view);

        final String menuId = getIntent().getStringExtra("menu_id");

        //Toast.makeText(this, "MENU ID " + menuId, Toast.LENGTH_SHORT).show();

        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);

                loadDrinkList(menuId);
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(true);

                loadDrinkList(menuId);
            }
        });

        loadDrinkList(menuId);


    }

    private void loadDrinkList(String menuId) {

        final List<Drink> drinks = new ArrayList<>();

        firebaseFirestore.collection("Menu/" + menuId + "/Drinks")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());

                                //Toast.makeText(HomeActivity.this, "name: " + document.get("name"), Toast.LENGTH_SHORT).show();
                                Drink drink = document.toObject(Drink.class).withId(document.getId());
                                drinks.add(drink);

                            }

                            displayDrinkList(drinks);

                        }

                    }

                });
    }

    private void displayDrinkList(List<Drink> drinks) {
        DrinkAdapter adapter = new DrinkAdapter(this,drinks);
        drinkRecycler.setAdapter(adapter);
        refreshLayout.setRefreshing(false);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
    }
}
