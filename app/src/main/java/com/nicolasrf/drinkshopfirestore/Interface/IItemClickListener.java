package com.nicolasrf.drinkshopfirestore.Interface;

import android.view.View;

/**
 * Created by Nicolas on 20/05/2018.
 */

public interface IItemClickListener {
    void onClick(View v);
}
