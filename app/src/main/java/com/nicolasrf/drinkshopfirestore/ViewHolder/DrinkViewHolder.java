package com.nicolasrf.drinkshopfirestore.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.drinkshopfirestore.Interface.IItemClickListener;
import com.nicolasrf.drinkshopfirestore.R;

/**
 * Created by Nicolas on 20/05/2018.
 */

public class DrinkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public ImageView productImageView;
    public TextView drinkNameTextView, drinkPriceTextView;

    IItemClickListener itemClickListener;

    public ImageView addToCartImageView, favoriteImageView;

    public void setItemClickListener(IItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public DrinkViewHolder(View itemView) {
        super(itemView);

        productImageView = itemView.findViewById(R.id.product_image_view);
        drinkNameTextView = itemView.findViewById(R.id.drink_name_text_view);
        drinkPriceTextView = itemView.findViewById(R.id.drink_price_text_view);
        addToCartImageView = itemView.findViewById(R.id.add_cart_button);
        favoriteImageView = itemView.findViewById(R.id.favorite_button);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v);
    }
}
