package com.nicolasrf.drinkshopfirestore.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.drinkshopfirestore.Interface.IItemClickListener;
import com.nicolasrf.drinkshopfirestore.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public ImageView productImageView;
    public TextView menuNameTextView;

    IItemClickListener itemClickListener;

    public void setItemClickListener(IItemClickListener iItemClickListener) {
        this.itemClickListener = iItemClickListener;
    }

    public CategoryViewHolder(View itemView) {
        super(itemView);

        productImageView = itemView.findViewById(R.id.product_image_view);
        menuNameTextView = itemView.findViewById(R.id.menu_name_text_view);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v);
    }
}
